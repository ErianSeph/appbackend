FROM node:latest

RUN mkdir -p /src/app

WORKDIR /src/app

COPY package*.json ./

RUN npm install
RUN npm install nodemon -g --quiet 

COPY . .

EXPOSE 8000

CMD [ "nodemon",  "src/server.js"]
