const mongoose = require('mongoose');

var ArticleSchema = new mongoose.Schema({
    _id: String,
    story_title: String,
    title: String,
    url: String,
    story_url: String,
    author: String,
    created_at: Date,
    deleted: {
        type: Boolean,
        default: false
    }
});

var Article = mongoose.model('Article', ArticleSchema);

module.exports = Article;
