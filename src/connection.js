const mongoose = require("mongoose");
const config = require('./config')

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);

let mongoDB = config.database.mongoURL;
let _db;
mongoose.Promise = global.Promise;

mongoose.connection.once("open", () => console.log('Mongoose connected!'));

mongoose.connection.on("error", console.error.bind(console, 'Connection error:'));

const connectDB = async () => {
    _db = mongoose.connect(mongoDB);
}

const getDB = () => _db;

const disconnectDB = () => {
    mongoose.disconnect();
}

module.exports = {
    connectDB,
    getDB,
    disconnectDB
}
