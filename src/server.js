const express = require('express');
const bodyParser = require('body-parser')
const MongoDBUtil = require('./connection')
const job = require('./cronTask')
const cors = require('cors');
const insertDummyData = require('./insertDummyData.js')

const app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());
app.use(require('./routes/article.router'));

let server = app.listen(8000, function () {
    console.log(`Listening on ${8000}`);
    MongoDBUtil.connectDB();
    insertDummyData();
});

job.start();

module.exports = {
    app,
    server
};
