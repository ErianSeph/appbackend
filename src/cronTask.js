const config = require('./config')
const CronJob = require('cron').CronJob
const Article = require('./models/article.model');
const request = require('request');
const moment = require('moment');

let handleRequest = async (error, response, body) => {
    if (error) {
        console.error(error.message);
        throw new Error('The request could not be handled')
    } else {
        const articles = body.hits;
        for (let articleInfo of articles) {
            const {
                story_title,
                title,
                url,
                story_url,
                author,
                created_at,
                objectID
            } = articleInfo;
            await Article.findById(objectID)
                .then(async document => {
                    if (document == null) {
                        var article = new Article({
                            _id: objectID,
                            story_title: story_title,
                            title: title,
                            url: url,
                            story_url: story_url,
                            author: author,
                            created_at: moment(created_at)
                        });
                        await article.save()
                            .then(() => console.log('Article saved correctly!'))
                            .catch(err => console.error(err));
                    }
                })
                .catch(err => {
                    console.error(err.message);
                    throw new Error(`Error trying to get an Article with ID ${objectID}`);
                })
        }
    }
}

const job = new CronJob(config.cron_task.time_expression, () => {
    request({
        url: 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
        json: true
    }, (error, response, body) => handleRequest(error, response, body));
});

module.exports = job;