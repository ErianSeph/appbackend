let config = {
    "database": {
        "mongoURL": "mongodb://mongo:27017/ArticleDB"
    },
    "cron_task": {
        "time_expression": "0 0 */1 * * *"
    }
}

module.exports = config;
