const express = require('express');

const router = express.Router();

const {
    getArticle,
    getArticles,
    deleteArticle,
    updateArticle,
    createArticle
} = require('../controllers/article.controller');

router
    .route('/')
    .get((req, res) => res.json({
        message: "Welcome!"
    }))

router
    .route('/articles')
    .get(getArticles)
    .post(createArticle);

router
    .route('/articles/:id')
    .put(updateArticle)
    .get(getArticle)
    .delete(deleteArticle);

module.exports = router;