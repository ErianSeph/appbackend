let dummyData = [{
        _id: "22259456",
        deleted: false,
        story_title: "Babashka – A Clojure babushka for the grey areas of Bash",
        title: null,
        url: null,
        story_url: "https://github.com/borkdude/babashka",
        author: "lwb",
        created_at: "2020-02-06T19:34:25.000+00:00"
    },
    {
        _id: "22247382",
        deleted: false,
        story_title: null,
        title: "Why Docker? What’s all the hype about?",
        url: null,
        story_url: null,
        author: "maximization",
        created_at: "2020-02-05T15:16:06.000+00:00"
    },
    {
        _id: "22249998",
        deleted: false,
        story_title: "Ask HN: Who wants to be hired? (February 2020)",
        title: null,
        url: null,
        story_url: null,
        author: "hlve",
        created_at: "2020-02-05T19:04:10.000+00:00"
    }
];

module.exports = dummyData;
