const Article = require('./models/article.model');
let dummyData = require('./data/dummyData.js')

let insertDummyData = async () => {
    let currentDocuments = await Article.find({});
    if (currentDocuments.length == 0) {
        Article.insertMany(dummyData)
            .then(() => console.log('Dummy data inserted correctly!'))
            .catch((err) => {
                console.error(err.message);
                throw new Error('Initial data could not be inserted.')
            })
    }
}

module.exports = insertDummyData;
