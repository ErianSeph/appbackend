const Article = require('../models/article.model');
const mongoose = require('mongoose');

const getArticle = async (req, res) => {
    await Article.findById(req.params.id)
        .then((article) => res.json(article))
        .catch(err => {
            console.error(err.message);
            throw new Error('Article requested could not be retrieved');
        })
};

let getArticles = async (req, res) => {
    await Article.find({
            deleted: false
        })
        .sort('-date')
        .exec()
        .then((articles) => res.json(articles))
        .catch(err => {
            console.error(err.message);
            throw new Error('List of articles could not be retrieved');
        })
};

const createArticle = async (req, res) => {
    const newArticle = new Article(req.body);
    await newArticle.save()
        .then(() => res.json({
            status: 'OK',
            message: 'Article created succesfully',
            newArticle
        }))
        .catch(err => {
            console.error(err.message);
            throw new Error('Article could not be created.')
        })
};

const deleteArticle = async (req, res) => {
    await Article.findByIdAndRemove(req.params.id)
        .then((articleDeleted) => res.json({
            status: 'OK',
            message: `Article ${articleDeleted} succesfully deleted`
        }))
        .catch(err => {
            console.error(err.message)
            throw new Error(`Article with id ${id} could not be deleted.`)
        })
};


const updateArticle = async (req, res) => {
    await Article.findOneAndUpdate({
                _id: req.params.id
            },
            req.body, {
                new: true
            }
        )
        .then((articleUpdated) => res.json({
            status: 'OK',
            message: `Article with Id ${articleUpdated._id} succesfully updated`,
            articleUpdated
        }))
        .catch(err => {
            console.error(err.message);
            throw new Error(`Article with id ${id} could not be updated.`);
        })
};

module.exports = {
    getArticle,
    getArticles,
    createArticle,
    deleteArticle,
    updateArticle
}