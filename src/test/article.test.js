process.env.NODE_ENV = 'test';

const mongoose = require('mongoose');
const job = require('../cronTask')
let Article = require('../models/article.model');

//dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let {
    app,
    server
} = require('../server');
let {
    disconnectDB
} = require('../connection')
let should = chai.should();

chai.use(chaiHttp);

describe('Articles tests', () => {
    beforeEach(function (done) {
        Article.deleteMany({})
            .then(() => done())
            .catch(err => console.error(err));
    });

    after(function (done) {
        disconnectDB();
        job.stop();
        server.close();
        done();
    })

    describe('GET method to article route', () => {
        it('it should get all the articles in the database', (done) => {
            chai.request(app)
                .get('/articles')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                })
        })
    })

    describe('POST method to article route', () => {
        it('it should add a new article', (done) => {
            let article = {
                _id: "1234",
                story_title: "testing story title",
                title: "testing title",
                url: "testing url",
                story_url: "testing story url",
                author: "testing author",
                created_at: "2020-02-05T19:04:10.000Z"
            }
            chai.request(app)
                .post('/articles')
                .send(article)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status').eql('OK');
                    res.body.should.have.property('message').eql('Article created succesfully');
                    res.body.should.have.property('newArticle').be.a('object');
                    res.body.newArticle.should.have.property('story_title').eql('testing story title');
                    res.body.newArticle.should.have.property('title').eql('testing title');
                    res.body.newArticle.should.have.property('url').eql('testing url');
                    res.body.newArticle.should.have.property('story_url').eql('testing story url');
                    res.body.newArticle.should.have.property('author').eql('testing author');
                    res.body.newArticle.should.have.property('created_at').eql('2020-02-05T19:04:10.000Z');
                    done();
                })
        })
    })

    describe('GET/:id route for articles', () => {
        it('it should get an article with the given id', (done) => {
            let article = new Article({
                _id: "1234",
                story_title: "testing story title",
                title: "testing title",
                url: "testing url",
                story_url: "testing story url",
                author: "testing author",
                created_at: "2020-02-05T19:04:10.000Z"
            })
            article.save()
                .then(() => {
                    chai.request(app)
                        .get(`/articles/${article.id}`)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('story_title').eql('testing story title');
                            res.body.should.have.property('title').eql('testing title');
                            res.body.should.have.property('url').eql('testing url');
                            res.body.should.have.property('story_url').eql('testing story url');
                            res.body.should.have.property('author').eql('testing author');
                            res.body.should.have.property('created_at').eql('2020-02-05T19:04:10.000Z');
                            res.body.should.have.property('_id').eql(article.id);
                            done();
                        })
                })
        })
    })

})
