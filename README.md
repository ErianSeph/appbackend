# Backend Article App

The backend was made with `express.js` and `mongoose` framework for mongoDB

### Launching the app for the first time with dummy data

This was made considering the first launch application as zero elements into the database. In that case, the database will be populated with dummy data contained inside `src/data/dummyData.js`.

### Testing

The testing can be executed with the command: `npm test`

### Coverage

The coverage can be obtained executing the command: `npm run test-coverage`. The coverage was made using [Istanbul library](https://www.npmjs.com/package/nyc). The results obtained in the local machine are the following:

![](src/data/coverage.png?raw=true)

### Running Docker Compose for backend and frontend

To compile the whole app, you need to clone the frontend of this [repository](https://gitlab.com/ErianSeph/articleappfrontend).
After, you have to clone this project, *you must have this structure folder*:

```
|
|_ articleappfrontend
|_ appbackend
```

Change folder into `appbackend` and execute the command: `docker-compose up`

The structure is mandatory due to docker compose file refers as a context to `./articleappfrontend` to build.